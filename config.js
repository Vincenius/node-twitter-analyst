module.exports = {
	consumerKey: 'yourAppKey',
	consumerSecret: 'yourAppSecret',
	bearerToken: 'yourAppToken'
}