/**
 * Created by Vincent on 09-Feb-18.
 */

// https://www.npmjs.com/package/sentiment
const Sentiment = require('sentiment');
const sentiment = new Sentiment();
const Twitter = require('twitter');
const readline = require('readline');
const config = require('./config');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// https://apps.twitter.com/
const client = new Twitter({
	consumer_key: config.consumerKey,
	consumer_secret: config.consumerSecret,
	bearer_token: config.bearerToken
});

rl.question('Please enter the word / phrase to be analysed ', (searchWord) => {
  const url = `search/tweets.json?q=${searchWord}&include_entities=true&count=100&result_type=popular`;

  client.get(url, function(error, tweets, response) {
	var totalRating = 0;
	var count = 0;
	
	for(var attributename in tweets.statuses) {
		var tweetText = tweets.statuses[attributename].text;
		var textRating = sentiment.analyze(tweetText);
		
		console.log(tweetText);
		console.log(textRating.score);
		console.log("--------");
		
		totalRating += parseFloat(textRating.score);
		count++;
	}	
	
	console.log(`Total Rating of ${totalRating} in ${count} Results`);
	console.log(`Average of ${totalRating / count}`);
  });

  rl.close();
});



	